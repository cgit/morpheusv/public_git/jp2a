Name:           jp2a
Version:        1.0.6
Release:        3%{?dist}
Group: System Environment/Shells
Summary:     An utility for converting JPEG images to ASCII   

License:        GPLv2 
URL:            http://jp2a.sourceforge.net/
Source0:        http://sourceforge.net/projects/jp2a/files/jp2a/1.0.6/%name-%version.tar.bz2
Buildrequires: libjpeg-devel libcurl-devel

%description
jp2a is a small command-line utility for converting JPEG images
to ASCII art.

%prep
%setup -q


%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%files
%doc AUTHORS ChangeLog README LICENSES
%{_bindir}/%{name}
%{_mandir}/man1/*


%changelog
* Sat Jun 5 2011 Andres Pascasio <morpheusv@gmail.com> - 1.0-3
- First Fedora spec compliant version, add libcurl-devel in Buildrequires

* Wed Jun 1 2011 Andres Pascasio <morpheusv@gmail.com> - 1.0-2
- First Fedora spec compliant version, several modification

* Sat May 28 2011 Andres Pascasio <morpheusv@gmail.com> - 1.0-1
- First Fedora spec compliant version
